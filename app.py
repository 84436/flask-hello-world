from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello, (containerized) World! I'm currently running in Docker on an EC2 instance, with configurations (docker-compose) backed by S3.</p>"
