# Python 3 on Alpine Linux as base
FROM python:alpine

# Working directory
WORKDIR /app

# Copy dependency list and install that first
COPY requirements.txt .
RUN pip install -r requirements.txt

# Copy the rest
COPY . .

# Run the entry point shell script
CMD ["sh", "start.sh"]
