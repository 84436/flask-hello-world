#!/bin/sh

# Set Flask to run in production mode
export FLASK_ENV=production

# Expose the application to all net iface
flask run \
    --host=0.0.0.0 \
    --port=80
